var selectedRow = null
const form = document.querySelector('form');
const td = document.querySelector('td');
const button = document.querySelector('button');
const input = document.getElementById('submit');
let itemsArray = localStorage.getItem('item') ? JSON.parse(localStorage.getItem('item')) : [];

localStorage.setItem('item', JSON.stringify(itemsArray));
const data = JSON.parse(localStorage.getItem('item'));

function onFormSubmit() {
    if (validate()) {
        var formData = {};
        formData["nama"] = document.getElementById("nama").value;
        formData["stok"] = document.getElementById("stok").value;
        formData["jual"] = document.getElementById("jual").value;
        
        if (selectedRow == null)
            insertNewRecord(formData);
        else
            updateRecord(formData);
        resetForm();
    }
};
function insertNewRecord(wow) {
    var table = document.getElementById("listbarang").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);
    
    itemsArray.push(wow);
    localStorage.setItem('item', JSON.stringify(itemsArray));
    const dataout = JSON.parse(localStorage.getItem('item'));
    
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = wow.nama;
    cell2 = newRow.insertCell(1);
    cell2.innerHTML = wow.stok;
    cell3 = newRow.insertCell(2);
    cell3.innerHTML = wow.jual;
    cell4 = newRow.insertCell(3);
    cell4.innerHTML = `<a onClick="onEdit(this)">Edit</a>
                       <a onClick="onDelete(this)">Delete</a>`;
    wow = "";
};

function resetForm() {
    document.getElementById("nama").value = "";
    document.getElementById("stok").value = "";
    document.getElementById("jual").value = "";
    selectedRow = null;
}

function onEdit(td) {
    selectedRow = td.parentElement.parentElement;
    document.getElementById("nama").value = selectedRow.cells[0].innerHTML;
    document.getElementById("stok").value = selectedRow.cells[1].innerHTML;
    document.getElementById("jual").value = selectedRow.cells[2].innerHTML;
}
function updateRecord(formData) {
    selectedRow.cells[0].innerHTML = formData.nama;
    selectedRow.cells[1].innerHTML = formData.stok;
    selectedRow.cells[2].innerHTML = formData.jual;
}

function onDelete(hh) {
    if (confirm('Are you sure to delete this record ?')) {
        row = hh.parentElement.parentElement;
        td.removeChild(row);
        resetForm();
    }
}

data.forEach(submit => {
    
});

function validate() {
    isValid = true;
    if (document.getElementById("nama").value == "") {
        isValid = false;
        document.getElementById("namaValidationError").classList.remove("hide");
    } else {
        isValid = true;
        if (!document.getElementById("namaValidationError").classList.contains("hide"))
            document.getElementById("namaValidationError").classList.add("hide");
    }
    return isValid;
}